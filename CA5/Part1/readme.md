# CA5 Part1 
## CI/CD Pipelines with Jenkins

### Goal
### The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle basic demo".


### 1. Create Folders and Readme File.

```
create folder 
C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794\CA5

$mkdir CA5

inside Folder
$mkdir Part1
create folder 
$mkdir Part1.1

create Readme.md
$touch readme.md
```

### 2. Create an issue on bitbucket

### 3. Install Jenkins 
Download jenkins.war [Jenkins]( https://www.jenkins.io/download/) 

### 4. Copy the application 
copy from gradle_basic_demo from CA2/part1 to CA5/part1

### 5. Configuring the pipeline
```
$docker network create jenkins
```

```
 $docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean
``` 

```
image
de75a8a7a5618247626821154e180a9091fd10529e6adbdf8583d257541fbc7d
```
password is generator
50e5f3c9186a46b595e35bd5a109bdf1

### 6. Create Jenkinsfile

## Define the following stages in your pipeline
Checkout, Assemble, Test

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794/src/master/CA5'
            }
        }
        stage('Assemble') {
            steps {
                dir ('CA2/part1/gradle_basic_demo')
                {
                        echo 'Assembling...'
                        script{
                            sh 'pwd'
                            sh 'chmod u+x ./gradlew'
                            sh './gradlew assemble'
                            sh './gradlew testClasses'
                        }
                }
            }
        }
        stage ('Test'){
            steps{
                dir ('CA2/part1/gradle_basic_demo')
                {
                    script {
                        echo 'Testing...'
                        sh 'pwd'
                        sh './gradlew check'
                        sh './gradlew test'
                    }

                }
            }
        }
        stage('Archiving') {
            steps {
                dir ('CA2/part1/gradle_basic_demo')
                {
                    echo 'Archiving...'
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }

    }

    post {
        always {
            junit '**/test-results/**/*.xml'
        }
    }
}
```

### 7. Docker run
http://localhost:8080/
build now 
```
$docker run 4a0cafce9ee2
```
![build]()
- 