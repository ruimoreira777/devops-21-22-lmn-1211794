# CA5 Part2
##CI/CD Pipelines with Jenkins
### Goal
### The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).


### 1. Create Folders and Readme File.

```
create folder 
C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794\CA5\Part2

$mkdir CA5
create folder 
$mkdir Part2

create Readme.md
$touch readme.md
```

### 2. Create an issue on bitbucket

### 3. Configuring Jenkins
The previous stages are exactly the same as the previous exercise. 
I will follow Part2 from previous assignment Part1.

### 4. Add Dockerfile from CA4

```
FROM tomcat:8-jdk8-temurin

RUN apt-get update -y 

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://ruimoreira777@bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794.git

WORKDIR /tmp/build/devops-21-22-lmn-1211794/CA4/Part2/tut-basic-gradle/

RUN chmod u+x gradlew

RUN ./gradlew clean build && cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -Rf /tmp/build/

EXPOSE 8080

```

### 5. JavaDoc
Javadoc. Generates the javadoc of the project and publish it in Jenkins. See the
publishHTML step for further information on how to archive/publish html reports.


```
 stage('Publish Javadoc') {
            steps {
                dir('CA5/Part2/gradle_basic_demo') {
                    echo 'Publishing JavaDocs'
                    bat './gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        keepAll: true,
                        reportDir: 'build/docs/javadoc/',
                        reportName: 'javadoc',
                        reportFiles: 'index.html',
                    ])

                }
            }
        }

```

### 6. Archive
Archives in Jenkins the archive files (generated during Assemble, i.e., the war file).

```
stage ('Archiving') {
            steps {
                dir('CA5/Part2/gradle_basic_demo') {
                    echo 'Archiving...'
                    archiveArtifacts 'build/libs/'
                }
            }
        }
```

### 7. Publish Image
Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

```
stage('Docker Image') {
            steps {
                echo 'Building Docker Image...'
                dir("CA5/Part2/gradle_basic_demo") {
                    script {
                        dockerImage = docker.build("rp777/devops-21-22-lmn-1211794:V0.${env.BUILD_ID}", ".")
                        docker.withRegistry("", 'dockerhub'){
                            dockerImage.push()
                        }
                    }
                }
            }
        }
    }
```

### Jenkinsfile final
```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794/src/master/CA5'
            }
        }


        stage('Assemble') {
                steps {
                    dir('CA5/Part2/gradle_basic_demo') {
                        echo 'Assembling Project...'
                        bat './gradlew clean build -x test'
                }
            }
        }

         stage('Test') {
            steps {
                dir('CA5/Part2/gradle_basic_demo') {
                    echo 'Testing'
                        bat './gradlew test'
                    }
                }
            }

        stage('Publish Javadoc') {
            steps {
                dir('CA5/Part2/gradle_basic_demo') {
                    echo 'Publishing JavaDocs'
                    bat './gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        keepAll: true,
                        reportDir: 'build/docs/javadoc/',
                        reportName: 'javadoc',
                        reportFiles: 'index.html',
                    ])

                }
            }
        }

        stage ('Archiving') {
            steps {
                dir('CA5/Part2/gradle_basic_demo') {
                    echo 'Archiving...'
                    archiveArtifacts 'build/libs/'
                }
            }
        }

        stage('Docker Image') {
            steps {
                echo 'Building Docker Image...'
                dir("CA5/Part2/gradle_basic_demo") {
                    script {
                        dockerImage = docker.build("rp777/devops-21-22-lmn-1211794:V0.${env.BUILD_ID}", ".")
                        docker.withRegistry("", 'dockerhub'){
                            dockerImage.push()
                        }
                    }
                }
            }
        }
    }
}
```

### 7. Build now
![](C:\Users\rui_l\Desktop\Screenshot 2022-06-08 165545.png)7.png)![build]()

### Image Create
![](C:\Users\rui_l\Desktop\Screenshot 2022-06-08 165657.png)![build]()

- 