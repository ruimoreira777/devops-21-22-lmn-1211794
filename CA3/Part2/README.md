
## Virtualization with Vagran - 

### 1. Create Folders and Readme File.
```
inside Folder Part3
create folder Part2
$ mkdir part2  
$ touch readme.md
```

### 2. Install Vagrant                           
Access to [Vagrant download](https://www.vagrantup.com/downloads) 
Version 2.2.19

To verify Vagrant installation 
```
$ vagrant -v
```

### 3. Start Virtual Machine commands               
Start vagrant
```
$ vagrant up
```

Start ssh using VM
```
$ vagrant ssh
```

Shutdown VM
```
$ vagrant halt
```

### 4. Edit the Vagrant file  
```                                 
$ nano Vagrantfile
```

### 5. Download and install Apache                               
Uncomment lines from the Vagrantfile from "config.vm-provision" onwards until "SHELL"

### 6.  Edit a port from the VM to the host machine 
uncomment lines from the Vagrantfile from "config.vm.network" "forwarded-port"  
```                               
change "host:8080" to "host:8010"
```                      

### 7.  Setup a static private IP on the VM                      
Uncomment lines from the Vagrantfile from "config.vm.network" "private_network"                             
Change "ip: 192.168.33.10" to "ip: 192.168.56.5"                                    
Set up a shared folder for the webserver pages   
                            
Uncomment lines from the Vagrantfile from "config.vm.synced_folder"                        
Change "../data", "/vagrant_data" to "./html", "/var/www/html"
Save changes to VagrantFile

Removed the box from my host machine                     
Vagrant box remove envimation/ubuntu-xenial

### 8. Edit application files
$ git clone https://ruimoreira777@bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794.git

Several changes were made following the repository: 

in build.gradle file, see line 30, edited:  
  ```               
from assembleScript = "run build"                                
to assembleScript = 'run webpack'     
```                                                                                                 
  
in package.json file, see line 28:   
```                             
added "watch": "webpack --watch -d" 
 ```                                                        

in build.gradle file:    
 ```                         
added id 'war' (see line 6)               
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat' (see line 25)    
 ```                            
  
  
added java.Class ServletInitializer and made the necessary changes in Class DatabaseLoader and Employee                             

```
 in application.properties file added:           
 server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT             
  spring.data.rest.base-path=/api                              
  spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE *(changed IP to "192.168.56.11" since it is the one defined in the Vagrant file)*                                              
  spring.datasource.driverClassName=org.h2.Driver                    
  spring.datasource.username=sa                         
  spring.datasource.password=                  
  spring.jpa.database-platform=org.hibernate.dialect.H2Dialect                 
  spring.jpa.hibernate.ddl-auto=update                       
  spring.h2.console.enabled=true                                
  spring.h2.console.path=/h2-console                       
  spring.h2.console.settings.web-allow-others=true                                     
```

in app.js file, see line 18, edited:  
 ```                       
from '/basic-0.0.1-SNAPSHOT..                 
to '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT..  
 ```                                            
  

in index.html file, see line 6, edited:             
  from <link rel="stylesheet" href="/main.css" />                 
  to <link rel="stylesheet" href="main.css" />                                        
  

in .gitignore file in app, copied the complete content from the professors repository                              


### 9. Install an ftp server 
```                          
$sudo apt install vsftpd
```                                    

### 10. Testing the VM db and Web  
```
vagrant up                     
vagrant reload    
vagrant halt   
```                    
- run URL http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ (web VM)                                                        
- run URL http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (db VM)                              





