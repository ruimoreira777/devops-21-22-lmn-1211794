# Part1 
## Introduction to Virtualization - 


## Virtual Box

What is Virtual Box?
" VirtualBox is a powerful x86 and AMD64/Intel64 virtualization product for enterprise as well as home use. Not only is VirtualBox an extremely feature rich, high performance product for enterprise customers, it is also the only professional solution that is freely available as Open Source Software under the terms of the GNU General Public License (GPL) version 2. See "About VirtualBox" for an introduction.<br>

Presently, VirtualBox runs on Windows, Linux, Macintosh, and Solaris hosts and supports a large number of guest operating systems including but not limited to Windows (NT 4.0, 2000, XP, Server 2003, Vista, Windows 7, Windows 8, Windows 10), DOS/Windows 3.x, Linux (2.4, 2.6, 3.x and 4.x), Solaris and OpenSolaris, OS/2, and OpenBSD.<br>

VirtualBox is being actively developed with frequent releases and has an ever growing list of features, supported guest operating systems and platforms it runs on. VirtualBox is a community effort backed by a dedicated company: everyone is encouraged to contribute while Oracle ensures the product always meets professional quality criteria.
With Gradle, the process is simplified, as it automatically takes all source files (.java or .xml),
applies the appropriate tools, and bundles them all into a single compressed file, the APK."<br>

[Virtual Box](https://www.virtualbox.org/)

##Goals
### Use VirtualBox, a free hypervisor that runs in many host operating systems, such as Windows, Linux and OSX<br>

### 1. Create Folders and Readme File.
```
create folder mkdir CA3
inside Folder Part3
create folder Part1
```

### 2. Install Virtual Box                           
Access to [Virtual Box](https://www.virtualbox.org/wiki/Downloads)<br>                        
choose VirtualBox 6.1.34 platform package Windows Hosts (for Windows users)                     

### 3. Create Virtual Machine                        
open Oracle VM Virtual Box<br>             
choose "New" on the Oracle VM Virtual Box Manager<br>                 
use a name for your Virtual Machine (mine: ubuntu-devops-CA3)<br>                   
choose memory (RAM) size (2048 MB) and "Create a virtual hard disk now"<br>                 
select  "VDI (VirtualBox Disk Image)" and select "Dynamically allocated"<br>                  
select "Create"<br>    

### 4. Edit Virtual Machine                                   
select  VM<br> --> "Settings"<br> --> "Storage" -->"Empty"-->"Optical Drive" disk icon  ---> mini iso file<br>                                     
[download the mini iso file](https://help.ubuntu.com/community/Installation/MinimalCD#A64-bit_PC_.28amd64.2C_x86_64.29_.28Recommended<br>                                            
Check the "Live CD/DVD" box<br>

### 5. Create Host-Only Network (Before Starting the VM)                              
Access to main menu and select "File"and then "Host Network Manager"<br>                                  
Go to VM and select "settings" in order to be able to enabled Network Adapter 2  as Host-Only Adaptar<br>                       
Select the newly created adapter ("VirtualBox Host-Only Ethernet Adapter")<br>
In the end with shoud check IP address range of this network: 192.168.56.1/24.


## 6. Setup Networking 

```
Update the packages repositories
$sudo apt update
Install the network tools:
$sudo apt install net-tools
Edit the network configuration file to setup the IP
$sudo nano /etc/netplan/01-netcfg.yaml

```                       

### 7. Edit the network configuration file                     
- Edit the network configuration file
```
sudo nano /etc/netplan/01-netcfg.yaml   
```
Adding the following contents to the file:                       
    - network:                 
        version: 2              
        renderer: network                       
        ethernets:                   
          enp0s3:                         
            dhcp4: yes                   
          enp0s8:                              
            addresses:                        
              - 192.168.56.5/24                                              
<br>  

### 8. - Network Utilities 
```
Apply the previous changes                           
$sudo netplan apply

Install openssh-server                                       
$sudo apt install openssh-server

Enable password authentication for ssh
$sudo nano /etc/ssh/sshd_config
uncomment the line PasswordAuthentication yes
$sudo service ssh restart
```

### 9. Install an ftp server 
```                          
$sudo apt install vsftpd
```                                    

### 10. Enable write access for vsftpd
```                                     
$sudo nano /etc/vsftpd.conf
```                                             
uncomment line "write_enable=YES"                                 
ctrl+x plus S (to exit nano and save changes)
  ```                                     
$ sudo service vsftpd restart
  ```                                                                

### 11. Use SSH to connect to the VM                                       
 ```
$ ssh rui_l@192.168.56.5
```                                            
                                                                                      
### 12. Install Git and JDK11
```                          
$ sudo apt install git
$ sudo apt install openjdk-11-jdk-headless  
``` 
                     
### 13. Cloning, Building and Running the Spring Tutorial
We are now ready to install and execute the Spring Tutorial application.
```
Make a clone of the application with one the following command:
$ git clone https://ruimoreira777@bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794.git

Change to the directory of the basic version
$ cd tut-react-and-spring-data-rest/basic

Build
$ ./mvnw install
$ ./mvnw spring-boot:run
```
Run host machine browser http://192.168.56.5:8080/



