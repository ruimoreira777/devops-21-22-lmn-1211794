# Part2
## Containers with Docker


## Goals
### The goal of this assignment is to use Docker to setup a containerized environment to
### execute your version of the gradle version of the spring basic tutorial application

### 1. Create Folders and Readme File.
Folder and readme file created already on Part1

### 2. Create an issue on bitbucket

### 3. Create docker-compose.yml file
```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24

```


### 4. Git clone docker-compose-spring-tut-demo.git
git clone https://bitbucket.org/atb/docker-compose-spring-tut-demo.git

### 5.  Copy the react-and-spring-data-rest-basic from CA3/part2
C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794\CA4\Part2\tut-basic-gradle

## 6. Remove .git
Remove files .git from tut-basic-gradle and docker-compose-spring-tut-demo.git                       

### 7. Edit port to 33
```
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```

### 9. Build compose build
```
$docker compose build
```

### 10. Run the docker images from docker-compose.yml 

```
$docker-compose up
```

### 11. Check images
```
$docker images
```

### 11. Tag images

```
$docker tag c1629f22ec75 rp777/devops-21-22-lmn-1211794:ca4part2Web
$docker tag 34535d5ca57a rp777/devops-21-22-lmn-1211794:ca4part2db

```

### 12. Push images
```
$docker push rp777/devops-21-22-lmn-1211794:ca4part2Web
$docker push rp777/devops-21-22-lmn-1211794:ca4part2db
```

### 13. Testing the web and db container
web container
```
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ 
```

db container
```
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
```








