# Part1 
##Introduction to Containers and Docker

##Goals
###The goal of the Part 1 of this assignment is to practice with Docker, creating docker
###images and running containers using the chat application from CA2

### 1. Create Folders and Readme File.

```
create folder 
C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794\CA4

$mkdir CA4

inside Folder
$mkdir Part1
create folder 
$mkdir Part1.1

create Readme.md
$touch readme.md
```

### 2. Create an issue on bitbucket

### 3. Install Install Docker Desktop
Access to [Docker](https://www.docker.com/)                       
Choose Docker Desktop - For running Docker in Windows machine.     

### 4. Create an account on Docker Hub

### 5. Create Dockerfile
create dockerfile and edit in intelliJ 
dockerfile add it to this Part1  

### 6. Create an image
```
$docker build -t Part1 .
```

### 5.  Create and run a container
```
$docker run --name firstContainer -p 59001:59001 -d Part1
```

## 6. Tagging the image
```
$docker tag d5a043a641b6 Part1
```                       

### 7. Publish in docker hub
```
$docker push rp777/firstcontainer:Part1
```

### 9. -  Execute the chat client on the host machine 
First git clone from https://ruimoreira777@bitbucket.org/luisnogueira/gradle_basic_demo.git)
```
$ ./gradlew runClient 
```

###Dockerfile commands

```
Download base image ubuntu 18.04
$FROM ubuntu:18.04

Run during build phase
$RUN apt-get update -y

install apache
$RUN apt-get install -y apache2

install git
$RUN apt-get install -y git

Install java version 11
$RUN apt-get install openjdk-11-jdk-headless -y

Clone repository
$RUN git clone https://ruimoreira777@bitbucket.org/luisnogueira/gradle_basic_demo.git

Change current working directory on the container
$WORKDIR gradle_basic_demo/

Execute permission
$RUN chmod u+x gradlew

Execute to build
$RUN ./gradlew clean build

Expose Port for the Application
$EXPOSE 59001

Execute the container after phase
$CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```





