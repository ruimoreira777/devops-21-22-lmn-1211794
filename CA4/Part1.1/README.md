# Part1.1
##Introduction to Containers and Docker
###Run Server

##Goals
###The goal of the Part 1 of this assignment is to practice with Docker, creating docker
###images and running containers using the chat application from CA2

### 1. Create Folders and Readme File.
Folder and readme file created already on Part1

### 2. Create an issue on bitbucket

### 3. Create Dockerfile
create dockerfile and edit in intelliJ 
dockerfile add it to this Part1.1

### 4. Create an image
```
$docker build -t Part1.1 .
```

### 5.  Create and run a container
```
$docker run --name firstContainer -p 59001:59001 -d Part1.1
```

## 6. Tagging the image
```
$docker tag 73549284afbb Part1.1
```                       

### 7. Publish in docker hub
```
$docker push rp777/firstcontainer:Part1.1
```

### 9. -  Execute the chat client on the host machine 
First git clone from https://ruimoreira777@bitbucket.org/luisnogueira/gradle_basic_demo.git)
```
$ ./gradlew runClient 
```

###Dockerfile commands

```
Download base image ubuntu 18.04
$FROM ubuntu:18.04

Run during build phase
$RUN apt-get update -y

Install apache
$RUN apt-get install -y apache2

Install java version 11
$RUN apt-get install openjdk-11-jdk-headless -y

#Copy the jar file
COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .

Expose Port for the Application
$EXPOSE 59001

Execute the container after phase
$CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

```





