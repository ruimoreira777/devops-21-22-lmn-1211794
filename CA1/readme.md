# Introduction to Git
##  Version Control with Git - The Basics

This projet contains part 1 (basic) of the tutorial react-and-spring-data-rest. See [react-and-spring-data-rest](https://spring.io/guides/tutorials/react-and-spring-data-rest/).


## Goals/Requirements

The purpose of this work is to understand the basic concepts of using Git software and understand how to work with version control. At the end of this work, I will have the ability to manage commits and branches that support version control.

At this point, you can do all the basic local Git operations — creating or cloning a repository, making changes, staging and committing those changes, and viewing the history of all the changes the repository has been through. Next, we'll cover Git's killer feature: its branching model.

## Installation
Before starting using git it is necessary to install Git.
[git download](https://gitforwindows.org/).

### Setup First Time Git Setup
```
git config -l 
```
-Used display the git configuration

### Setup Personal information
In order to use Git it is necessary to configure it with personal information, such as username and email:
```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

### Create repository
The creation of the repository is done through Bitbucket.
The repository was created with the name devops-21-22-LMN-1211794
[repository Bitbucktet](https://bitbucket.org/ruimoreira777/devops-21-22-lmn-1211794/src/master/).

### Creating Tasks
Tasks were created in Bitbucket in order to support the required tasks.

Issues
create issue

## First Part - commit

### 1. Create folder CA1 for each class assigment
Creating a folder in the directory : $ C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794
 ```
mkdir devops-21-22-LMN-1211794
```
This folder will be used to add the application to be managed and the readme.md file

### 1.1 Initializing a Repository in an Existing Directory

This command allows me to initialize the repository or create an existing one.
This creates a new subdirectory named .git that contains all of your necessary
repository files - a Git repository skeleton.

```
$ git init
```

### [.git - version control]
At this stage it is important to verify that before making git clone of the project, it is necessary to remove the existing .git file so that there are not two version controls

### 1.2 Create Readme.md file
Directory point where the local folder was created:
$ C:\Users\rui_l\Desktop\Switch\DevOps\devops-21-22-lmn-1211794

##### Create the readme.md file
```
$echo "This is a readme file." >> readme.md
$ git add readme.md
$ git commit -m 'initial project version'
```

### 1.3 Cloning an Existing Repository - Git clone
Command that allows copying the file to my local repository with the name of the directory tut-react-and-spring-data-rest.
```
$ git clone https://github.com/spring-guides/tut-react-and-spring-data-rest
```

### 1.4 Checking the Status of Your Files
Allows you to check and determine the status of the states
```
$ git status
```

message:
```
$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
```
Indicates that there is currently nothing to commit

### 2 Committing Changes- See Modifications

After cloning the project and adding the readme file, the commit process was started
```
$git diff
```
Let me see the differences before submitting.
```
$git add
```
This command was used to get a summary of which files have changes that are prepared for the next commit.
```
$ git commit
$ git commit -m "first commit"
```
First commit made with the changes made
```
$git status
```
To verify that everything was sent correctly

```
$ git push origin master
```
After verifying that everything was done correctly in the previous point, in order to send to Bitbucket it is necessary to push

### 3 Tagging
Tags created to easily check history
```
$ git tag v1.1.0
git push origin v1.1.0
```

### 4 Lets develop a new feature to add a new field to the application.
#### In this case, let's add a new field to record the years of the employee in the company (eg, jobYears).

In this phase, changes were made locally to the project code, such as adding record the jobYear and tests.

### 4.1 Should be committed and pushed and a new tag should be created (eg, v1.2.0)
```
$ git tag -a v1.2.0 -m "add a new field to the application"
$ git push origin v1.2.0
```

### 5 End of the assignment mark
After solving some bugs, a new tag was created and a new commit was made
```
$git add .
$git commit
$ git tag -a ca1-part1 -m "end first part"
$ git push origin ca1-part1
```

## Second Part - using branches
Branches allow you to develop features, fix bugs, or safely experiment with new ideas in a contained area of your repository.

>You should implement a simple scenario illustrating a simple git workflow.
The objective of this second part is to develop a set of techniques that allow understanding the resolution of branches.

## 1. Create new Branch
```
$ git branch email-field
```
Java code was added regarding the email information as well as the tests, before committing tests and debugging of the application were performed.

2.1
Allow me to enter the field I want to work.
```
$ git checkout email-field
````

Let me go back to the master.
```
$ git add .
$ git commit -a -m 'email-field add it'
$ git checkout master
````

Allows me to merge the code I had working by selecting the desired branch.
```
$ git merge email-field
```
Create new tag and push to repository.
```
$ git tag v1.3.0
$ git push v1.3.0
```

### 3. Create branches for fixing bugs "fix-invalid-email".

## 1 Create new Branch
```
$ git branch fix-invalid-email
```
Java code was added referring to job information with a valid email as well as tests, before committing tests and application debugging were performed.
two

Allow me to enter the field I want to work
```
2.1 $ git checkout fix-invalid-email
```

Let me go back to the master
```
$ git add .
$ git commit -a -m 'fix-invalid-email add it'
$ git checkout master
```

Allows me to merge the code I had working by selecting the desired branch
```
$ git merge fix-invalid-email
```

A new tag was created and pushed
```
$ git tag v1.3.1
$ git push v1.3.1
```

Finally, a new tag ca1-part2 and push all changes was created
```
$ git tag -a ca1-part2 -m "end second part"
$ git push origin ca1-part2
```


