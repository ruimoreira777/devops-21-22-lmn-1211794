package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.devtools.restart.RestartScope;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void EmployeeTestFullDetailsAddict(){

        String firstName = "Rui";
        String lastName ="Lima";
        String description="student";
        String email ="ipp@gmail.com";
        int jobYears= 2;

        Employee employee =  new Employee(firstName, lastName, description, jobYears, email);
        assertNotNull(employee);
    }


    @Test
    void EmployeeTestNull(){

        int jobYears= 2;

        String expected = "invalid name";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("", null, "Fist Projects", jobYears,"ip@gmail.com");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedLastName(){

        int jobYears= 2;

        String expected = "invalid lastName";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("Rui", null, "First project created", jobYears,"ip@gmail.com");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedLastNameNull(){

        int jobYears= 2;

        String expected = "invalid name";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee(null, null, "First project created", jobYears, "fp@gmail.com");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedDescription(){

        int jobYears= 2;

        String expected = "invalid description";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("Rui", "Lima", "", jobYears, "ip@gmail.com");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedInavlidNNumber(){

        int jobYears= -1;
        String expected = "invalid jobYears";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("Rui", "Lima", "First Project crate",-1,"ip@gmail.com");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedInvalidEmail(){


        String expected = "Invalid email";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("Rui", "Lima", "First Project crate",2, "");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void EmployeeTestFailedInvalidEmailWithNumbers(){

        String expected = "Invalid email";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee =  new Employee("Rui", "Lima", "First Project crate", 2,"");
        });
        //Assert
        assertEquals(expected, exception.getMessage());
    }

}