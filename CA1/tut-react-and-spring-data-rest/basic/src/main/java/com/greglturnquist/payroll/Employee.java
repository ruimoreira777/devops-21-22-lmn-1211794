/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private int jobYears;
	private String email;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, int jobYears, String email) {

		validateFirstName(firstName);
		validateLastName(lastName);
		validatedescription(description);
		validatedescription(description);
		validatejobYears(jobYears);
		validateEmail(email);


		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.jobYears = jobYears;
		this.email =email;

	}


	private void validateEmail(String email){
		//add regex to support reference an email must have the "@" sign
		if (email != null){
			String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
					+ "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(email);
			if (matcher.matches()) return;
		}
		throw new IllegalArgumentException("Invalid email");
	}

	private void validateFirstName(String firstName){
		if (firstName != null){
			firstName = firstName.trim();
			if (!firstName.isEmpty()) return;
		}
		throw new IllegalArgumentException("invalid name");
	}

	private void validateLastName(String lastName){

		if (lastName != null){
			lastName = lastName.trim();
			if (!lastName.isEmpty()) return;
		}
		throw new IllegalArgumentException("invalid lastName");

	}
	private void validatedescription(String description){
		if (description != null){
			description = description.trim();
			if (!description.isEmpty()) return;
		}
		throw new IllegalArgumentException("invalid description");
	}

	private void validatejobYears(int jobYears){
		if (jobYears< 0){
			throw new IllegalArgumentException("invalid jobYears");
		}

	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getJobYears() {
		return jobYears;
	}

	public void setJobYears(int jobYears) {
		this.jobYears = jobYears;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return jobYears == employee.jobYears && Objects.equals(id, employee.id) && Objects.equals(firstName, employee.firstName) && Objects.equals(lastName, employee.lastName) && Objects.equals(description, employee.description) && Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, jobYears, email);
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
				", jobYears='" + jobYears + '\'' +
				", email='" + email + '\'' +
			'}';
	}
}
// end::code[]
